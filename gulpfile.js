var gulp = require('gulp'),
	sass = require('gulp-sass');

gulp.task('sassbuild', function() {

		return gulp.src('./sass/**/*.scss')
			// .pipe(plumber({
			// 	'errorHandler': onError
			// }))
			// .pipe(plugins.sourcemaps.init()) // can't get them to work in conjunction with bless
			.pipe(sass({
				'outputStyle': 'expanded'
			}))
			// .pipe(autoprefixer({
			// 	'browsers': pkg.sass.autoprefixer.browsers
			// }))
			// .pipe(plugins.sourcemaps.write('maps')) // can't get them to work in conjunction with bless
			// .pipe(bless())
			.pipe(gulp.dest('./public/css/'));
			// .pipe(notify({
			// 	'message': 'SASS: ' + o.file + ' build complete',
			// 	'onLast': true // otherwise the notify will be fired for each file in the pipe
			// }));
});
